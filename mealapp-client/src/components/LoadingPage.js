import React from "react";
import { Spinner } from "@blueprintjs/core";
const LoadingPage = () => (
  <div className="loader">
    <Spinner />
  </div>
);

export default LoadingPage;
