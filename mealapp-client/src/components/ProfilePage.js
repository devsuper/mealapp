import React from "react";
import { connect } from "react-redux";
import { Card, Elevation, Intent } from "@blueprintjs/core";
import { Col } from "react-simple-flex-grid";
import { AppToaster } from "./Toaster";
import ProfileForm from "./ProfileForm";
import LoadingPage from "./LoadingPage";
import { startEditProfile, startGetProfile } from "../actions/auth";

export class ProfilePage extends React.Component {
  componentDidMount() {
    this.props.getProfile();
  }
  onSubmit = profile => {
    this.props.editProfile(profile).then(
      message => {
        AppToaster.show({ message: message, intent: Intent.SUCCESS });
      },
      error => {
        AppToaster.show({ message: error.errorMsg, intent: Intent.WARNING });
      }
    );
  };
  render() {
    return (
      <div>
        <br />
        <Col xs={{ span: 6, offset: 3 }}>
          <Card elevation={Elevation.ONE}>
            <h1>My profile</h1>
            {this.props.busy && <LoadingPage />}
            {!this.props.busy && (
              <ProfileForm {...this.props.user} onSubmit={this.onSubmit} />
            )}
          </Card>
        </Col>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    busy: state.auth.busy
  };
};

const mapDispatchToProps = dispatch => ({
  editProfile: (id, user) => dispatch(startEditProfile(id, user)),
  getProfile: () => dispatch(startGetProfile())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfilePage);
