import React from "react";
import {
  Button,
  H1,
  FormGroup,
  InputGroup,
  Card,
  Elevation,
  Intent
} from "@blueprintjs/core";
import { AppToaster } from "./Toaster";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { startLogin } from "../actions/auth";

export class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      error: ""
    };
  }
  onUserNameChange = e => {
    const username = e.target.value;
    this.setState(() => ({ username }));
  };
  onPasswordChange = e => {
    const password = e.target.value;
    this.setState(() => ({ password }));
  };
  onSubmit = e => {
    e.preventDefault();
    this.props.startLogin(this.state.username, this.state.password).then(
      message => {
        AppToaster.show({ message: message, intent: Intent.SUCCESS });
      },
      error => {
        AppToaster.show({ message: error.errorMsg, intent: Intent.WARNING });
      }
    );
  };
  render() {
    return (
      <div className="box-layout">
        <Card interactive={false} elevation={Elevation.FOUR}>
          <H1>Meal App</H1>
          <hr />
          {this.props.error && <p>{this.props.error}</p>}
          {this.state.error && <p>{this.state.error}</p>}
          <form onSubmit={this.onSubmit}>
            <FormGroup label="Username" labelFor="username" inline={true}>
              <InputGroup
                id="username"
                required={true}
                placeholder="Enter your username"
                type={"text"}
                onChange={this.onUserNameChange}
              />
            </FormGroup>
            <FormGroup label="Password" labelFor="password" inline={true}>
              <InputGroup
                id="password"
                required={true}
                placeholder="Enter your password..."
                type={"password"}
                onChange={this.onPasswordChange}
              />
            </FormGroup>
            <Button type="submit" fill={true} large={true} intent="primary">
              Login
            </Button>
          </form>
          <hr />
          <Link to="/signup">or Sign up!</Link>
        </Card>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { error: state.auth.error };
}

const mapDispatchToProps = dispatch => ({
  startLogin: (username, password) => dispatch(startLogin(username, password))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
