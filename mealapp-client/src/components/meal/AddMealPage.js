import React from "react";
import { connect } from "react-redux";
import { AppToaster } from "../Toaster";
import MealForm from "./MealForm";
import { startAddMeal } from "../../actions/meal";
import { Card, Elevation, Intent } from "@blueprintjs/core";
import { Col } from "react-simple-flex-grid";

export class AddMealPage extends React.Component {
  onSubmit = meal => {
    this.props.addMeal(meal).then(
      message => {
        AppToaster.show({ message: message, intent: Intent.SUCCESS });
        this.props.history.push("/meals");
      },
      error => {
        AppToaster.show({ message: error.errorMsg, intent: Intent.WARNING });
      }
    );
  };
  render() {
    return (
      <div>
        <br />
        <Col xs={{ span: 6, offset: 3 }}>
          <Card elevation={Elevation.ONE}>
            <h1>Add Meal</h1>
            <hr />
            <MealForm onSubmit={this.onSubmit} />
          </Card>
        </Col>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  addMeal: meal => dispatch(startAddMeal(meal))
});

export default connect(
  undefined,
  mapDispatchToProps
)(AddMealPage);
