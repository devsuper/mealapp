import React from "react";
import { connect } from "react-redux";
import { Card, Elevation, Intent } from "@blueprintjs/core";
import { Col } from "react-simple-flex-grid";
import MealForm from "./MealForm";
import { startEditMeal, startRemoveMeal } from "../../actions/meal";
import { AppToaster } from "../Toaster";

export class EditMealPage extends React.Component {
  onSubmit = meal => {
    this.props.editMeal(this.props.meal.id, meal).then(
      message => {
        AppToaster.show({ message: message, intent: Intent.SUCCESS });
        this.props.history.push("/meals");
      },
      error => {
        AppToaster.show({ message: error.errorMsg, intent: Intent.WARNING });
      }
    );
  };
  onRemove = () => {
    this.props.removeMeal(this.props.meal.id).then(
      message => {
        AppToaster.show({ message: message, intent: Intent.SUCCESS });
        this.props.history.push("/meals");
      },
      error => {
        AppToaster.show({ message: error.errorMsg, intent: Intent.WARNING });
      }
    );
  };
  render() {
    return (
      <div>
        <br />
        <Col xs={{ span: 6, offset: 3 }}>
          <Card elevation={Elevation.ONE}>
            <h1>Edit Meal</h1>
            <hr />
            <MealForm
              meal={this.props.meal}
              onSubmit={this.onSubmit}
              onRemove={this.onRemove}
            />
          </Card>
        </Col>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  meal: state.meals.list.find(
    meal => meal.id === parseInt(props.match.params.id)
  )
});

const mapDispatchToProps = (dispatch, props) => ({
  editMeal: (id, meal) => dispatch(startEditMeal(id, meal)),
  removeMeal: data => dispatch(startRemoveMeal(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditMealPage);
