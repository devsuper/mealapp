import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faUser,
  faUtensils,
  faCalendar,
  faEdit
} from "@fortawesome/free-solid-svg-icons";
import { H4, H6, Card, Elevation } from "@blueprintjs/core";
import moment from "moment";

const MealItem = ({
  id,
  description,
  calories,
  exceedsDailyCalories,
  user,
  date,
  url
}) => (
  <div>
    <Card interactive={true} elevation={Elevation.TWO}>
      <H4>
        <Link to={`${url}/edit/${id}`}>
          {description} <FontAwesomeIcon icon={faEdit} />
        </Link>
      </H4>
      <H6>
        <FontAwesomeIcon icon={faUser} /> by {user.name}
      </H6>
      <p>
        <FontAwesomeIcon icon={faUtensils} /> {calories}{" "}
        <FontAwesomeIcon icon={faCalendar} /> {moment(date).calendar()}
      </p>
      <hr className={exceedsDailyCalories ? "card-danger" : "card-success"} />
    </Card>
    <br />
  </div>
);

export default MealItem;
