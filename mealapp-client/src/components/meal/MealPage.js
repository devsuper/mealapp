import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import MealList from "./MealList";
import LoadingPage from "../LoadingPage";
import MealListFilters from "./MealListFilters";
import { startGetMeals } from "../../actions/meal";
import {
  Button,
  H1,
  FormGroup,
  InputGroup,
  ControlGroup,
  Card,
  Elevation
} from "@blueprintjs/core";
import { Row, Col } from "react-simple-flex-grid";

export class MealPage extends React.Component {
  componentDidMount() {
    this.props.startGetMeals();
  }
  render() {
    return (
      <div>
        {this.props.busy && <LoadingPage />}
        {!this.props.busy && (
          <Row>
            <Col span={3}>
              <MealListFilters />
            </Col>
            <Col span={1}>
              <hr className="hrv" />
            </Col>
            <Col span={8}>
              <MealList url={this.props.match.url} />
            </Col>
          </Row>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    busy: state.meals.busy
  };
};

const mapDispatchToProps = dispatch => ({
  startGetMeals: () => dispatch(startGetMeals())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MealPage);
