import React from "react";
import { connect } from "react-redux";
import MealItem from "./MealItem";
import MealTotals from "./MealTotals";
import selectMeals from "../../selectors/meals";

export const MealList = props => (
  <div>
    <h1>My Meals</h1>
    {props.meals.length > 0 && <MealTotals meals={props.meals} />}
    <hr />
    {props.meals.length === 0 ? (
      <p>No meals</p>
    ) : (
      props.meals.map(meal => {
        return <MealItem key={meal.id} {...meal} url={props.url} />;
      })
    )}
  </div>
);

const mapStateToProps = state => {
  return {
    meals: selectMeals(state.meals.list, state.mealfilters)
  };
};

export default connect(mapStateToProps)(MealList);
