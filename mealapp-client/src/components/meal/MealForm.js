import React from "react";
import moment from "moment";
import {
  Button,
  H1,
  FormGroup,
  InputGroup,
  NumericInput
} from "@blueprintjs/core";
import { DatePicker, TimePrecision } from "@blueprintjs/datetime";

export default class MealForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      description: props.meal ? props.meal.description : "",
      numOfCalories: props.meal ? props.meal.calories : 0,
      date: props.meal ? moment(props.meal.date).toDate() : moment().toDate()
    };
  }
  onDescriptionChange = e => {
    const description = e.target.value;
    this.setState(() => ({ description }));
  };
  onCaloriesChange = (valueAsNumber, valueAsString) => {
    this.setState(() => ({ numOfCalories: valueAsNumber }));
  };
  onDateChange = date => {
    if (date) {
      this.setState(() => ({ date }));
    }
  };
  onFocusChange = ({ focused }) => {
    this.setState(() => ({ calendarFocused: focused }));
  };
  onSubmit = e => {
    e.preventDefault();
    this.props.onSubmit({
      description: this.state.description,
      numOfCalories: this.state.numOfCalories,
      date: this.state.date
    });
  };
  onRemove = e => {
    e.preventDefault();
    this.props.onRemove();
  };
  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <FormGroup label="Description" labelFor="description">
            <InputGroup
              id="description"
              minLength={4}
              maxLength={40}
              required={true}
              placeholder="Enter your description..."
              type={"text"}
              value={this.state.description}
              onChange={this.onDescriptionChange}
            />
          </FormGroup>
          <FormGroup label="Calories" labelFor="numOfCalories">
            <NumericInput
              id="numOfCalories"
              min={0}
              minorStepSize={1}
              required={true}
              placeholder="Enter your Calories..."
              type={"text"}
              value={this.state.numOfCalories}
              onValueChange={this.onCaloriesChange}
            />
          </FormGroup>
          <FormGroup label="Created at:" labelFor="createdAt">
            <DatePicker
              id="createdAt"
              onChange={this.onDateChange}
              value={this.state.date}
              timePrecision={TimePrecision.MINUTE}
            />
          </FormGroup>
          <hr />
          <Button type="submit" fill={true} large={true} intent="primary">
            Submit
          </Button>
          <br />
          {this.props.meal && (
            <Button
              type="button"
              onClick={this.onRemove}
              fill={true}
              large={true}
              intent="Danger"
            >
              Remove
            </Button>
          )}
        </form>
      </div>
    );
  }
}
