import React from "react";
import { connect } from "react-redux";
import selectMealsTotal from "../../selectors/meals-total";
import selectMeals from "../../selectors/meals";

export const MealTotals = ({ mealsCount, mealsTotal }) => {
  const mealWord = mealsCount === 1 ? "meal" : "meals";
  return (
    <div>
      <h4>
        Viewing {mealsCount} {mealWord} totalling {mealsTotal} calories
      </h4>
    </div>
  );
};

const mapStateToProps = (state, props) => {
  const selectedMeals = selectMeals(props.meals, state.mealfilters);
  return {
    mealsCount: props.meals.length,
    mealsTotal: selectMealsTotal(selectedMeals)
  };
};

export default connect(mapStateToProps)(MealTotals);
