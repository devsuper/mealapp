import React from "react";
import moment from "moment";
import { connect } from "react-redux";
import { HTMLSelect, FormGroup, InputGroup } from "@blueprintjs/core";
import { DatePicker, TimePrecision, TimePicker } from "@blueprintjs/datetime";

import {
  setTextFilter,
  sortByDate,
  sortByCalories,
  setFromDate,
  setToDate,
  setFromTime,
  setToTime
} from "../../actions/mealfilters";

export class MealListFilters extends React.Component {
  onFromDateChange = date => {
    this.props.setFromDate(date);
  };
  onToDateChange = date => {
    this.props.setToDate(date);
  };
  onFromTimeChange = date => {
    this.props.setFromTime(date);
  };
  onToTimeChange = date => {
    this.props.setToTime(date);
  };
  onTextChange = e => {
    this.props.setTextFilter(e.target.value);
  };
  onSortChange = e => {
    if (e.target.value === "date") {
      this.props.sortByDate();
    } else if (e.target.value === "calories") {
      this.props.sortByCalories();
    }
  };
  render() {
    return (
      <div>
        <h1>Meal Filters</h1>
        <hr />
        <FormGroup label="Find by name" labelFor="text">
          <InputGroup
            type="text"
            id="text"
            value={this.props.mealfilters.text}
            onChange={this.onTextChange}
          />
        </FormGroup>
        <FormGroup label="Sort by " labelFor="sortBy" inline={true}>
          <HTMLSelect
            id="sortBy"
            value={this.props.mealfilters.sortBy}
            onChange={this.onSortChange}
          >
            <option value="date">Date</option>
            <option value="calories">Calories</option>
          </HTMLSelect>
        </FormGroup>
        <FormGroup label="Date from: " labelFor="fromDate">
          <DatePicker
            id="fromDate"
            onChange={this.onFromDateChange}
            maxDate={this.props.mealfilters.toDate}
            value={this.props.mealfilters.fromDate}
            canClearSelection={false}
          />
        </FormGroup>
        <FormGroup label="Date to: " labelFor="toDate">
          <DatePicker
            id="toDate"
            minDate={this.props.mealfilters.fromDate}
            onChange={this.onToDateChange}
            value={this.props.mealfilters.toDate}
            canClearSelection={false}
          />
        </FormGroup>
        <FormGroup label="Time from: " labelFor="fromTime" inline={true}>
          <TimePicker
            onChange={this.onFromTimeChange}
            value={this.props.mealfilters.fromTime}
            showArrowButtons={true}
          />
        </FormGroup>
        <FormGroup label="Time to: " labelFor="toTime" inline={true}>
          <TimePicker
            onChange={this.onToTimeChange}
            value={this.props.mealfilters.toTime}
            showArrowButtons={true}
          />
        </FormGroup>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  mealfilters: state.mealfilters
});

const mapDispatchToProps = dispatch => ({
  setTextFilter: text => dispatch(setTextFilter(text)),
  sortByDate: () => dispatch(sortByDate()),
  sortByCalories: () => dispatch(sortByCalories()),
  setFromDate: fromDate => dispatch(setFromDate(fromDate)),
  setToDate: toDate => dispatch(setToDate(toDate)),
  setFromTime: fromTime => dispatch(setFromTime(fromTime)),
  setToTime: toTime => dispatch(setToTime(toTime))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MealListFilters);
