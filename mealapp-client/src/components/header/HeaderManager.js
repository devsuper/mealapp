import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Navbar, Alignment, Button } from "@blueprintjs/core";
import { startLogout } from "../../actions/auth";

export const HeaderManager = ({ startLogout }) => (
  <Navbar className="header">
    <div className="container">
      <Navbar.Group align={Alignment.LEFT}>
        <Navbar.Heading>MEAL APP</Navbar.Heading>
        <Navbar.Divider />
        <Button className="bp3-minimal" icon="plus">
          <Link to="/meals/create">Create Meal</Link>
        </Button>
        <Button className="bp3-minimal">
          <Link to="/meals">My Meals</Link>
        </Button>
        <Navbar.Divider />
        <Button className="bp3-minimal" icon="plus">
          <Link to="/users/create">Create User</Link>
        </Button>
        <Button className="bp3-minimal">
          <Link to="/users">Users</Link>
        </Button>
      </Navbar.Group>
      <Navbar.Group align={Alignment.RIGHT}>
        <Button className="bp3-minimal" icon="user">
          <Link to="/profile">Profile</Link>
        </Button>
        <Button className="bp3-minimal" icon="log-out" onClick={startLogout} />
      </Navbar.Group>
    </div>
  </Navbar>
);

const mapDispatchToProps = dispatch => ({
  startLogout: () => dispatch(startLogout())
});

export default connect(
  undefined,
  mapDispatchToProps
)(HeaderManager);
