import React from "react";
import { connect } from "react-redux";
import { DatePicker, TimePicker } from "@blueprintjs/datetime";
import {
  Button,
  HTMLSelect,
  FormGroup,
  InputGroup,
  MenuItem
} from "@blueprintjs/core";
import { Select } from "@blueprintjs/select";
import {
  setTextFilter,
  sortByDate,
  sortByCalories,
  setFromDate,
  setToDate,
  setFromTime,
  setToTime,
  setUser
} from "../../actions/mealfilters";

class UserMealListFilters extends React.Component {
  onFromDateChange = date => {
    this.props.setFromDate(date);
  };
  onToDateChange = date => {
    this.props.setToDate(date);
  };
  onFromTimeChange = date => {
    this.props.setFromTime(date);
  };
  onToTimeChange = date => {
    this.props.setToTime(date);
  };
  onTextChange = e => {
    this.props.setTextFilter(e.target.value);
  };
  handleclick = createdBy => {
    this.props.setUser(createdBy);
  };
  itemRenderer = (user, { handleClick }) => {
    return (
      <MenuItem
        key={user.id}
        label={user.name}
        text={user.name}
        onClick={handleClick}
        shouldDismissPopover={true}
      />
    );
  };
  onSortChange = e => {
    if (e.target.value === "date") {
      this.props.sortByDate();
    } else if (e.target.value === "calories") {
      this.props.sortByCalories();
    }
  };
  render() {
    return (
      <div>
        <h1>User Meal Filters</h1>
        <hr />
        <FormGroup label="Find by name" labelFor="text">
          <InputGroup
            type="text"
            id="text"
            value={this.props.mealfilters.text}
            onChange={this.onTextChange}
          />
        </FormGroup>
        <FormGroup label="Sort by " labelFor="sortBy" inline={true}>
          <HTMLSelect
            id="sortBy"
            value={this.props.mealfilters.sortBy}
            onChange={this.onSortChange}
          >
            <option value="date">Date</option>
            <option value="calories">Calories</option>
          </HTMLSelect>
        </FormGroup>
        <FormGroup label="Created By:" labelFor="createdBy" inline={true}>
          <Select
            id="createdBy"
            items={this.props.users}
            filterable={false}
            itemRenderer={this.itemRenderer}
            onItemSelect={this.handleclick}
          >
            <Button
              text={
                this.props.mealfilters.user
                  ? this.props.mealfilters.user.name
                  : "- Select a user -"
              }
              rightIcon="caret-down"
            />
          </Select>
        </FormGroup>
        <FormGroup label="Date from: " labelFor="fromDate">
          <DatePicker
            id="fromDate"
            onChange={this.onFromDateChange}
            maxDate={this.props.mealfilters.toDate}
            value={this.props.mealfilters.fromDate}
            canClearSelection={false}
          />
        </FormGroup>
        <FormGroup label="Date to: " labelFor="toDate">
          <DatePicker
            id="toDate"
            minDate={this.props.mealfilters.fromDate}
            onChange={this.onToDateChange}
            value={this.props.mealfilters.toDate}
            canClearSelection={false}
          />
        </FormGroup>
        <FormGroup label="Time from: " labelFor="fromTime" inline={true}>
          <TimePicker
            onChange={this.onFromTimeChange}
            value={this.props.mealfilters.fromTime}
            showArrowButtons={true}
          />
        </FormGroup>
        <FormGroup label="Time to: " labelFor="toTime" inline={true}>
          <TimePicker
            onChange={this.onToTimeChange}
            value={this.props.mealfilters.toTime}
            showArrowButtons={true}
          />
        </FormGroup>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  mealfilters: state.mealfilters
});

const mapDispatchToProps = dispatch => ({
  setTextFilter: text => dispatch(setTextFilter(text)),
  sortByDate: () => dispatch(sortByDate()),
  sortByCalories: () => dispatch(sortByCalories()),
  setFromDate: fromDate => dispatch(setFromDate(fromDate)),
  setToDate: toDate => dispatch(setToDate(toDate)),
  setFromTime: fromTime => dispatch(setFromTime(fromTime)),
  setToTime: toTime => dispatch(setToTime(toTime)),
  setUser: user => dispatch(setUser(user))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserMealListFilters);
