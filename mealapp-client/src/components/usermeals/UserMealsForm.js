import React from "react";
import moment from "moment";
import {
  Button,
  H1,
  FormGroup,
  InputGroup,
  NumericInput,
  MenuItem
} from "@blueprintjs/core";
import { DatePicker, TimePrecision } from "@blueprintjs/datetime";
import { Select } from "@blueprintjs/select";

export default class UsersMealsForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      description:
        props.meal && props.meal.description ? props.meal.description : "",
      numOfCalories:
        props.meal && props.meal.calories ? props.meal.calories : 0,
      date:
        props.meal && props.meal.date
          ? moment(props.meal.date).toDate()
          : moment().toDate(),
      users: props.users,
      createdBy: props.meal && props.meal.user ? props.meal.user : ""
    };
  }
  onDescriptionChange = e => {
    const description = e.target.value;
    this.setState(() => ({ description }));
  };
  onCaloriesChange = (valueAsNumber, valueAsString) => {
    this.setState(() => ({ numOfCalories: valueAsNumber }));
  };
  onDateChange = date => {
    if (date) {
      this.setState(() => ({ date }));
    }
  };
  handleclick = createdBy => {
    this.setState(() => ({ createdBy }));
  };
  onFocusChange = ({ focused }) => {
    this.setState(() => ({ calendarFocused: focused }));
  };
  itemRenderer = (user, { handleClick }) => {
    return (
      <MenuItem
        key={user.id}
        label={user.name}
        text={user.name}
        onClick={handleClick}
        shouldDismissPopover={true}
      />
    );
  };

  onSubmit = e => {
    e.preventDefault();
    this.props.onSubmit({
      description: this.state.description,
      numOfCalories: this.state.numOfCalories,
      date: this.state.date,
      userId: this.state.createdBy.id
    });
  };

  onRemove = e => {
    e.preventDefault();
    this.props.onRemove();
  };

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <FormGroup label="Description" labelFor="description" inline={true}>
            <InputGroup
              id="description"
              minLength={4}
              maxLength={40}
              required={true}
              placeholder="Enter your description..."
              type={"text"}
              value={this.state.description}
              onChange={this.onDescriptionChange}
            />
          </FormGroup>
          <FormGroup label="Calories" labelFor="numOfCalories" inline={true}>
            <NumericInput
              id="numOfCalories"
              min={0}
              minorStepSize={1}
              required={true}
              placeholder="Enter your Calories..."
              type={"text"}
              value={this.state.numOfCalories}
              onValueChange={this.onCaloriesChange}
            />
          </FormGroup>
          <FormGroup label="Created By:" labelFor="createdBy" inline={true}>
            <Select
              id="createdBy"
              items={this.props.users}
              filterable={false}
              required={true}
              itemRenderer={this.itemRenderer}
              onItemSelect={this.handleclick}
            >
              <Button
                text={
                  this.state.createdBy
                    ? this.state.createdBy.name
                    : "- Select a user -"
                }
                rightIcon="caret-down"
              />
            </Select>
          </FormGroup>
          <FormGroup label="Created at:" labelFor="createdAt" inline={true}>
            <DatePicker
              id="createdAt"
              onChange={newDate => this.onDateChange(newDate)}
              value={this.state.date}
              timePrecision={TimePrecision.MINUTE}
            />
          </FormGroup>
          <hr />
          <Button type="submit" fill={true} large={true} intent="primary">
            Submit
          </Button>
          <br />
          {this.props.meal && (
            <Button
              type="button"
              onClick={this.onRemove}
              fill={true}
              large={true}
              intent="Danger"
            >
              Remove
            </Button>
          )}
        </form>
      </div>
    );
  }
}
