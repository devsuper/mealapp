import React from "react";
import { connect } from "react-redux";
import { AppToaster } from "../Toaster";
import { Card, Elevation, Intent } from "@blueprintjs/core";
import { Col } from "react-simple-flex-grid";
import UserMealsForm from "./UserMealsForm";
import { startGetUsers } from "../../actions/user";

import {
  startEditUserMeal,
  startRemoveUserMeal
} from "../../actions/usermeals";

export class EditUserMealPage extends React.Component {
  onSubmit = meal => {
    this.props.editUserMeal(this.props.meal.id, meal).then(
      message => {
        AppToaster.show({ message: message, intent: Intent.SUCCESS });
        this.props.history.push("/users/meals");
      },
      error => {
        AppToaster.show({ message: error.errorMsg, intent: Intent.WARNING });
      }
    );
  };
  onRemove = () => {
    this.props.removeUserMeal(this.props.meal.id).then(
      message => {
        AppToaster.show({ message: message, intent: Intent.SUCCESS });
        this.props.history.push("/users/meals");
      },
      error => {
        AppToaster.show({ message: error.errorMsg, intent: Intent.WARNING });
      }
    );
  };
  componentDidMount() {
    this.props.startGetUsers().then(
      () => {},
      error => {
        AppToaster.show({ message: error.errorMsg, intent: Intent.WARNING });
      }
    );
  }
  render() {
    return (
      <div>
        {this.props.busy && <LoadingPage />}
        {!this.props.busy && (
          <Col xs={{ span: 6, offset: 3 }}>
            <Card elevation={Elevation.ONE}>
              <h1>Edit User Meal</h1>
              <hr />
              <UserMealsForm
                meal={this.props.meal}
                users={this.props.users}
                onSubmit={this.onSubmit}
                onRemove={this.onRemove}
              />
            </Card>
          </Col>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  meal: state.usersmeals.list.find(
    meal => meal.id === parseInt(props.match.params.id)
  ),
  users: state.users.list
});

const mapDispatchToProps = (dispatch, props) => ({
  editUserMeal: (id, user) => dispatch(startEditUserMeal(id, user)),
  removeUserMeal: id => dispatch(startRemoveUserMeal(id)),
  startGetUsers: () => dispatch(startGetUsers())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditUserMealPage);
