import React from "react";
import { connect } from "react-redux";
import { AppToaster } from "../Toaster";
import UserMealsForm from "./UserMealsForm";
import { startAddUserMeal } from "../../actions/usermeals";
import { Card, Elevation, Intent } from "@blueprintjs/core";
import { Col } from "react-simple-flex-grid";
import { startGetUsers } from "../../actions/user";

export class AddMealPage extends React.Component {
  onSubmit = meal => {
    this.props.startAddUserMeal(meal).then(
      message => {
        AppToaster.show({ message: message, intent: Intent.SUCCESS });
        this.props.history.push("/users/meals");
      },
      error => {
        AppToaster.show({ message: error.errorMsg, intent: Intent.WARNING });
      }
    );
  };
  componentDidMount() {
    this.props.startGetUsers().then(
      () => {},
      error => {
        AppToaster.show({ message: error.errorMsg, intent: Intent.WARNING });
      }
    );
  }
  render() {
    return (
      <div>
        <br />
        {this.props.busy && <LoadingPage />}
        {!this.props.busy && (
          <Col xs={{ span: 6, offset: 3 }}>
            <Card elevation={Elevation.ONE}>
              <h1>Add User Meal</h1>
              <hr />
              <UserMealsForm
                users={this.props.users}
                onSubmit={this.onSubmit}
              />
            </Card>
          </Col>
        )}
      </div>
    );
  }
}
const mapStateToProps = (state, props) => ({
  users: state.users.list
});

const mapDispatchToProps = dispatch => ({
  startAddUserMeal: meal => dispatch(startAddUserMeal(meal)),
  startGetUsers: () => dispatch(startGetUsers())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddMealPage);
