import React from "react";
import UserMealsItem from "./UserMealsItem";
import MealTotals from "../meal/MealTotals";

export const UserMealsList = props => (
  <div>
    <h1>User Meals List</h1>
    {props.usersmeals.length > 0 && <MealTotals meals={props.usersmeals} />}
    <hr />
    {props.usersmeals.length === 0 ? (
      <p>No user meals</p>
    ) : (
      props.usersmeals.map(usermeal => {
        return <UserMealsItem key={usermeal.id} {...usermeal} />;
      })
    )}
  </div>
);

export default UserMealsList;
