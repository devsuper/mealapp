import React from "react";
import { connect } from "react-redux";
import { AppToaster } from "../Toaster";
import UserMealListFilters from "./UserMealListFilters";
import UserMealsList from "./UserMealsList";
import LoadingPage from "../LoadingPage";
import { startGetUserMeals } from "../../actions/usermeals";
import { startGetUsers } from "../../actions/user";
import selectMeals from "../../selectors/user-meals";
import { Intent } from "@blueprintjs/core";
import { Row, Col } from "react-simple-flex-grid";

export class UserMealPage extends React.Component {
  componentWillMount() {
    this.props.startGetUserMeals().then(
      () => {},
      error => {
        console.log("Error user meal page", error);
        AppToaster.show({ message: error.errorMsg, intent: Intent.WARNING });
      }
    );

    this.props.startGetUsers().then(
      () => {},
      error => {
        console.log("Error user meal page", error);
        AppToaster.show({ message: error.errorMsg, intent: Intent.WARNING });
      }
    );
  }
  render() {
    return (
      <div>
        {this.props.busy && <LoadingPage />}
        {!this.props.busy && (
          <Row>
            <Col span={3}>
              <UserMealListFilters users={this.props.users} />
            </Col>
            <Col span={1}>
              <hr className="hrv" />
            </Col>
            <Col span={8}>
              <UserMealsList usersmeals={this.props.usersmeals} />
            </Col>
          </Row>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    usersmeals: selectMeals(state.usersmeals.list, state.mealfilters),
    users: [{ id: -1, name: "ALL" }, ...state.users.list],
    busy: state.usersmeals.busy && state.users.busy
  };
};

const mapDispatchToProps = dispatch => ({
  startGetUserMeals: () => dispatch(startGetUserMeals()),
  startGetUsers: () => dispatch(startGetUsers())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserMealPage);
