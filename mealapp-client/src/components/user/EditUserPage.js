import React from "react";
import { connect } from "react-redux";
import { AppToaster } from "../Toaster";
import UserForm from "./UserForm";
import { Card, Elevation, Intent } from "@blueprintjs/core";
import { Col } from "react-simple-flex-grid";
import { startEditUser, startRemoveUser } from "../../actions/user";

export class EditUserPage extends React.Component {
  onSubmit = user => {
    this.props.editUser(this.props.user.id, user).then(
      message => {
        AppToaster.show({ message: message, intent: Intent.SUCCESS });
        this.props.history.push("/users");
      },
      error => {
        AppToaster.show({ message: error.errorMsg, intent: Intent.WARNING });
      }
    );
  };
  onRemove = () => {
    this.props.removeUser(this.props.user.id).then(
      message => {
        AppToaster.show({ message: message, intent: Intent.SUCCESS });
        this.props.history.push("/users");
      },
      error => {
        AppToaster.show({ message: error.errorMsg, intent: Intent.WARNING });
      }
    );
  };
  render() {
    return (
      <div>
        <br />
        <Col xs={{ span: 6, offset: 3 }}>
          <Card elevation={Elevation.ONE}>
            <h1>Edit User</h1>
            <hr />
            <UserForm
              user={this.props.user}
              onSubmit={this.onSubmit}
              onRemove={this.onRemove}
            />
          </Card>
        </Col>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  user: state.users.list.find(
    user => user.id === parseInt(props.match.params.id)
  )
});

const mapDispatchToProps = (dispatch, props) => ({
  editUser: (id, user) => dispatch(startEditUser(id, user)),
  removeUser: id => dispatch(startRemoveUser(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditUserPage);
