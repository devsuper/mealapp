import React from "react";
import { HTMLSelect, FormGroup, InputGroup, Button } from "@blueprintjs/core";

export default class UserForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.user ? props.user.name : "",
      username: props.user ? props.user.username : "",
      email: props.user ? props.user.email : "",
      password: "",
      role: props.user ? props.user.role : "ROLE_USER",
      error: ""
    };
  }
  onPasswordChange = e => {
    const password = e.target.value;
    this.setState(() => ({ password }));
  };
  onNameChange = e => {
    const name = e.target.value;
    this.setState(() => ({ name }));
  };
  onUserNameChange = e => {
    const username = e.target.value;
    this.setState(() => ({ username }));
  };
  onRoleChange = e => {
    const role = e.target.value;
    this.setState(() => ({ role }));
  };
  onEmailChange = e => {
    const email = e.target.value;
    this.setState(() => ({ email }));
  };
  onSubmit = e => {
    e.preventDefault();
    this.props.onSubmit({
      name: this.state.name,
      username: this.state.username,
      password: this.state.password,
      email: this.state.email,
      role: this.state.role
    });
  };
  onRemove = e => {
    e.preventDefault();
    this.props.onRemove();
  };
  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <FormGroup label="Name" labelFor="name" inline={true}>
            <InputGroup
              id="name"
              minLength={4}
              maxLength={40}
              required={true}
              placeholder="Enter your name..."
              type={"text"}
              value={this.state.name}
              onChange={this.onNameChange}
            />
          </FormGroup>
          <FormGroup label="Username" labelFor="username" inline={true}>
            <InputGroup
              id="username"
              minLength={3}
              maxLength={15}
              required={true}
              placeholder="Enter your username..."
              type={"text"}
              value={this.state.username}
              onChange={this.onUserNameChange}
            />
          </FormGroup>
          <FormGroup label="Password" labelFor="password" inline={true}>
            <InputGroup
              id="password"
              minLength={6}
              maxLength={20}
              required={true}
              placeholder="Enter your password..."
              type={"password"}
              onChange={this.onPasswordChange}
            />
          </FormGroup>
          <FormGroup label="Email" labelFor="email" inline={true}>
            <InputGroup
              id="email"
              maxLength={40}
              required={true}
              placeholder="Enter your email..."
              type={"email"}
              value={this.state.email}
              onChange={this.onEmailChange}
            />
          </FormGroup>
          <FormGroup label="Role" labelFor="role" inline={true}>
            <HTMLSelect
              id="role"
              value={this.state.role}
              onChange={this.onRoleChange}
            >
              <option value="ROLE_USER">User</option>
              <option value="ROLE_MANAGER">Manager</option>
              <option value="ROLE_ADMIN">Admin</option>
            </HTMLSelect>
          </FormGroup>
          <Button type="submit" fill={true} large={true} intent="primary">
            Submit
          </Button>
          <br />
          {this.props.user && (
            <Button
              type="button"
              onClick={this.onRemove}
              fill={true}
              large={true}
              intent="Danger"
            >
              Remove
            </Button>
          )}
        </form>
      </div>
    );
  }
}
