import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faUser,
  faUtensils,
  faCalendar,
  faEdit,
  faEnvelope,
  faCircle
} from "@fortawesome/free-solid-svg-icons";
import { H4, Card, Elevation } from "@blueprintjs/core";

const UserItem = ({ id, name, username, email, role, maxCaloriesPerDay }) => (
  <div>
    <Card interactive={true} elevation={Elevation.TWO}>
      <H4>
        <Link to={`users/edit/${id}`}>
          {name} <FontAwesomeIcon icon={faEdit} />
        </Link>
      </H4>
      <p>
        <p>
          <FontAwesomeIcon icon={faUser} /> Username : {username}
        </p>
        <p>
          <FontAwesomeIcon icon={faEnvelope} /> Email : {email}
        </p>
        <p>
          <FontAwesomeIcon icon={faCircle} /> Role : {role}
        </p>
        <p>
          <FontAwesomeIcon icon={faUtensils} /> Max calories per day:
          {maxCaloriesPerDay}
        </p>
      </p>
    </Card>
    <br />
  </div>
);

export default UserItem;
