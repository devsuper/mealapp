import React from "react";
import UserItem from "./UserItem";

export const UserList = props => (
  <div>
    <h2>User List</h2>
    {props.users.length === 0 ? (
      <p>No users</p>
    ) : (
      props.users.map(user => {
        return <UserItem key={user.id} {...user} />;
      })
    )}
  </div>
);

export default UserList;
