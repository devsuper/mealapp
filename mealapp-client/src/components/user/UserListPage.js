import React from "react";
import { connect } from "react-redux";
import { AppToaster } from "../Toaster";
import UserList from "./UserList";
import LoadingPage from "../LoadingPage";
import { startGetUsers } from "../../actions/user";
import { Intent } from "@blueprintjs/core";
import { Row, Col } from "react-simple-flex-grid";

export class UserListPage extends React.Component {
  componentDidMount() {
    this.props.startGetUsers().then(
      () => {},
      error => {
        AppToaster.show({ message: error.errorMsg, intent: Intent.WARNING });
      }
    );
  }
  render() {
    return (
      <div>
        {this.props.busy && <LoadingPage />}
        {!this.props.busy && (
          <Row>
            <Col span={12}>
              <UserList users={this.props.users} />
            </Col>
          </Row>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    users: state.users.list,
    busy: state.users.busy
  };
};

const mapDispatchToProps = dispatch => ({
  startGetUsers: () => dispatch(startGetUsers())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserListPage);
