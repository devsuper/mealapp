import React from "react";
import { connect } from "react-redux";
import { AppToaster } from "../Toaster";
import UserForm from "./UserForm";
import { startAddUser } from "../../actions/user";
import { Card, Elevation, Intent } from "@blueprintjs/core";
import { Col } from "react-simple-flex-grid";

export class AddUserPage extends React.Component {
  onSubmit = user => {
    this.props.startAddUser(user).then(
      message => {
        AppToaster.show({ message: message, intent: Intent.SUCCESS });
        this.props.history.push("/users");
      },
      error => {
        AppToaster.show({ message: error.errorMsg, intent: Intent.WARNING });
      }
    );
  };
  render() {
    return (
      <div>
        <br />
        <Col xs={{ span: 6, offset: 3 }}>
          <Card elevation={Elevation.ONE}>
            <h1>Add User</h1>
            <hr />
            <UserForm onSubmit={this.onSubmit} />
          </Card>
        </Col>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  startAddUser: user => dispatch(startAddUser(user))
});

export default connect(
  undefined,
  mapDispatchToProps
)(AddUserPage);
