import React from "react";
import { AppToaster } from "./Toaster";
import {
  Button,
  H1,
  FormGroup,
  InputGroup,
  Intent,
  Card,
  Elevation
} from "@blueprintjs/core";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { startSignUpRequst } from "../actions/auth";

export class SignUpPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      username: "",
      password: "",
      email: "",
      error: ""
    };
  }
  onNameChange = e => {
    const name = e.target.value;
    this.setState(() => ({ name }));
  };
  onUserNameChange = e => {
    const username = e.target.value;
    this.setState(() => ({ username }));
  };
  onPasswordChange = e => {
    const password = e.target.value;
    this.setState(() => ({ password }));
  };
  onEmailChange = e => {
    const email = e.target.value;
    this.setState(() => ({ email }));
  };
  onSubmit = e => {
    e.preventDefault();
    this.props
      .startSignUpRequst({
        name: this.state.name,
        username: this.state.username,
        password: this.state.password,
        email: this.state.email
      })
      .then(
        result => {
          AppToaster.show({ message: result, intent: Intent.SUCCESS });
          this.props.history.push("/");
        },
        error => {
          AppToaster.show({ message: error.errorMsg, intent: Intent.WARNING });
        }
      );
  };
  render() {
    return (
      <div className="box-layout">
        <Card interactive={false} elevation={Elevation.FOUR}>
          <H1>Meal App</H1>
          <hr />
          {this.props.error && <p>{this.props.error}</p>}
          {this.state.error && <p>{this.state.error}</p>}
          <form onSubmit={this.onSubmit}>
            <FormGroup label="Name" labelFor="name" inline={true}>
              <InputGroup
                id="name"
                minLength={4}
                maxLength={40}
                required={true}
                placeholder="Enter your name..."
                type={"text"}
                onChange={this.onNameChange}
              />
            </FormGroup>
            <FormGroup label="Username" labelFor="username" inline={true}>
              <InputGroup
                id="username"
                minLength={3}
                maxLength={15}
                required={true}
                placeholder="Enter your username..."
                type={"text"}
                onChange={this.onUserNameChange}
              />
            </FormGroup>
            <FormGroup label="Password" labelFor="password" inline={true}>
              <InputGroup
                id="password"
                minLength={6}
                maxLength={20}
                required={true}
                placeholder="Enter your password..."
                type={"password"}
                onChange={this.onPasswordChange}
              />
            </FormGroup>
            <FormGroup label="Email" labelFor="email" inline={true}>
              <InputGroup
                id="email"
                maxLength={40}
                placeholder="Enter your email..."
                type={"email"}
                onChange={this.onEmailChange}
              />
            </FormGroup>
            <Button
              type="submit"
              fill={true}
              large={true}
              intent="primary"
              disabled={!!this.props.busy}
            >
              Signup
            </Button>
          </form>
          <Link to="/">Sign in</Link>
        </Card>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  startSignUpRequst: user => dispatch(startSignUpRequst(user))
});

export default connect(
  undefined,
  mapDispatchToProps
)(SignUpPage);
