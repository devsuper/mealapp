import React from "react";
import { NumericInput, FormGroup, InputGroup, Button } from "@blueprintjs/core";

export default class ProfileForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.name,
      username: props.username,
      email: props.email,
      maxCaloriesPerDay: props.maxCaloriesPerDay,
      password: ""
    };
  }
  onPasswordChange = e => {
    const password = e.target.value;
    this.setState(() => ({ password }));
  };
  onNameChange = e => {
    const name = e.target.value;
    this.setState(() => ({ name }));
  };
  onUserNameChange = e => {
    const username = e.target.value;
    this.setState(() => ({ username }));
  };
  onRoleChange = e => {
    const role = e.target.value;
    this.setState(() => ({ role }));
  };
  onEmailChange = e => {
    const email = e.target.value;
    this.setState(() => ({ email }));
  };
  onCaloriesChange = (valueAsNumber, valueAsString) => {
    this.setState(() => ({ maxCaloriesPerDay: valueAsNumber }));
  };
  onSubmit = e => {
    e.preventDefault();
    this.props.onSubmit({
      name: this.state.name,
      username: this.state.username,
      password: this.state.password,
      email: this.state.email,
      maxCaloriesPerDay: this.state.maxCaloriesPerDay
    });
  };
  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <FormGroup label="Name" labelFor="name" inline={true}>
            <InputGroup
              id="name"
              minLength={4}
              maxLength={40}
              required={true}
              placeholder="Enter your name..."
              type={"text"}
              value={this.state.name}
              onChange={this.onNameChange}
            />
          </FormGroup>
          <FormGroup label="Username" labelFor="username" inline={true}>
            <InputGroup
              id="username"
              minLength={3}
              maxLength={15}
              required={true}
              placeholder="Enter your username..."
              type={"text"}
              value={this.state.username}
              onChange={this.onUserNameChange}
            />
          </FormGroup>
          <FormGroup label="Password" labelFor="password" inline={true}>
            <InputGroup
              id="password"
              minLength={6}
              maxLength={20}
              required={true}
              placeholder="Enter your password..."
              type={"password"}
              onChange={this.onPasswordChange}
            />
          </FormGroup>
          <FormGroup label="Email" labelFor="email" inline={true}>
            <InputGroup
              id="email"
              maxLength={40}
              placeholder="Enter your email..."
              type={"email"}
              value={this.state.email}
              onChange={this.onEmailChange}
            />
          </FormGroup>
          <FormGroup
            label="Daily maximum of calories"
            labelFor="maxCaloriesPerDay"
          >
            <NumericInput
              id="maxCaloriesPerDay"
              min={0}
              minorStepSize={1}
              required={true}
              type={"text"}
              value={this.state.maxCaloriesPerDay}
              onValueChange={this.onCaloriesChange}
            />
          </FormGroup>
          <Button type="submit" fill={true} large={true} intent="primary">
            Submit
          </Button>
        </form>
      </div>
    );
  }
}
