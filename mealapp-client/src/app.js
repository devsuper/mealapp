import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { login } from "./actions/auth";
import AppRouter, { history } from "./routers/AppRouter";
import configureStore from "./store/configureStore";
import "normalize.css/normalize.css";
import "./styles/styles.scss";
import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/datetime/lib/css/blueprint-datetime.css";
import LoadingPage from "./components/LoadingPage";
import "react-simple-flex-grid/lib/main.css";
import { apiConstants } from "./constants/api";

const store = configureStore();
const jsx = (
  <Provider store={store}>
    <AppRouter />
  </Provider>
);

const renderApp = () => {
  ReactDOM.render(jsx, document.getElementById("app"));
};

ReactDOM.render(<LoadingPage />, document.getElementById("app"));

if (
  sessionStorage.getItem(apiConstants.ACCESS_TOKEN) &&
  sessionStorage.getItem(apiConstants.ACCESS_ROLES)
) {
  store.dispatch(
    login(
      sessionStorage.getItem(apiConstants.ACCESS_TOKEN),
      sessionStorage.getItem(apiConstants.ACCESS_ROLES)
    )
  );
  renderApp();
} else {
  renderApp();
}
