import React from "react";
import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";
import { apiConstants } from "../constants/api";

export const PublicRoute = ({
  isAuthenticated,
  component: Component,
  ...rest
}) => (
  <Route
    {...rest}
    component={props =>
      isAuthenticated ? <Redirect to="/meals" /> : <Component {...props} />
    }
  />
);

const mapStateToProps = state => ({
  isAuthenticated: !!(sessionStorage.getItem(apiConstants.ACCESS_TOKEN) != null)
});

export default connect(mapStateToProps)(PublicRoute);
