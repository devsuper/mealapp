import React from "react";
import { connect } from "react-redux";
import { Router, Route, Switch, Link, NavLink } from "react-router-dom";
import createHistory from "history/createBrowserHistory";
import LoginPage from "../components/LoginPage";
import SignUpPage from "../components/SignUpPage";
import ProfilePage from "../components/ProfilePage";

import MealPage from "../components/meal/MealPage";
import AddMealPage from "../components/meal/AddMealPage";
import EditMealPage from "../components/meal/EditMealPage";

import UserListPage from "../components/user/UserListPage";
import AddUserPage from "../components/user/AddUserPage";
import EditUserPage from "../components/user/EditUserPage";

import UserMealPage from "../components/usermeals/UserMealPage";
import AddUserMealPage from "../components/usermeals/AddUserMealPage";
import EditUserMealPage from "../components/usermeals/EditUserMealPage";

import NotFoundPage from "../components/NotFoundPage";

import PrivateRoute from "./PrivateRoute";
import PublicRoute from "./PublicRoute";
import { hasRole } from "../selectors/auth";

export const history = createHistory();

const AppRouter = ({ isAdmin, isUser, isManager }) => (
  <Router history={history}>
    <div>
      <Switch>
        <PublicRoute path="/" component={LoginPage} exact={true} />
        <PublicRoute path="/signup" component={SignUpPage} exact={true} />
        <PrivateRoute path="/meals" component={MealPage} exact={true} />
        <PrivateRoute
          path="/meals/create"
          component={AddMealPage}
          exact={true}
        />
        <PrivateRoute
          path="/meals/edit/:id"
          component={EditMealPage}
          exact={true}
        />
        <PrivateRoute path="/users" component={UserListPage} exact={true} />

        <PrivateRoute
          path="/users/create"
          component={AddUserPage}
          exact={true}
        />

        <PrivateRoute
          path="/users/edit/:id"
          component={EditUserPage}
          exact={true}
        />

        <PrivateRoute
          path="/users/meals"
          component={UserMealPage}
          exact={true}
        />

        <PrivateRoute
          path="/users/meals/create"
          component={AddUserMealPage}
          exact={true}
        />
        <PrivateRoute
          path="/users/meals/edit/:id"
          component={EditUserMealPage}
          exact={true}
        />

        <PrivateRoute path="/profile" component={ProfilePage} exact={true} />

        <Route component={NotFoundPage} />
      </Switch>
    </div>
  </Router>
);

const mapStateToProps = state => ({
  isUser: !!state.auth.uid && hasRole(state.auth.roles, ["ROLE_USER"]),
  isAdmin: !!state.auth.uid && hasRole(state.auth.roles, ["ROLE_ADMIN"]),
  isManager: !!state.auth.uid && hasRole(state.auth.roles, ["ROLE_MANAGER"])
});

export default connect(mapStateToProps)(AppRouter);
