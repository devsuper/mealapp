import React from "react";
import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";
import { apiConstants } from "../constants/api";
import HeaderUser from "../components/header/HeaderUser";
import HeaderManager from "../components/header/HeaderManager";
import HeaderAdmin from "../components/header/HeaderAdmin";
import { hasRole } from "../selectors/auth";

export const PrivateRoute = ({
  isAuthenticated,
  isAdmin,
  isUser,
  isManager,
  component: Component,
  ...rest
}) => (
  <Route
    {...rest}
    component={props =>
      isAuthenticated ? (
        <div>
          {isUser && <HeaderUser />}
          {isAdmin && <HeaderAdmin />}
          {isManager && <HeaderManager />}
          <div className="container">
            <Component {...props} />
          </div>
        </div>
      ) : (
        <Redirect to="/" />
      )
    }
  />
);

const mapStateToProps = state => ({
  isAuthenticated: !!(
    sessionStorage.getItem(apiConstants.ACCESS_TOKEN) != null
  ),
  isUser: !!state.auth.uid && hasRole(state.auth.roles, ["ROLE_USER"]),
  isAdmin: !!state.auth.uid && hasRole(state.auth.roles, ["ROLE_ADMIN"]),
  isManager: !!state.auth.uid && hasRole(state.auth.roles, ["ROLE_MANAGER"])
});

export default connect(mapStateToProps)(PrivateRoute);
