export const apiConstants = {
  API_BASE_URL: process.env.SERVER_URL
    ? process.env.SERVER_URL
    : "http://localhost:5000/api",
  ACCESS_TOKEN: "accessToken",
  ACCESS_ROLES: "accessRoles"
};
