import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import authReducer from "../reducers/auth";
import mealReducer from "../reducers/meal";
import mealFiltersReducer from "../reducers/mealfilters";
import userReducer from "../reducers/user";
import userMealsReducer from "../reducers/usermeals";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
  const store = createStore(
    combineReducers({
      auth: authReducer,
      meals: mealReducer,
      mealfilters: mealFiltersReducer,
      users: userReducer,
      usersmeals: userMealsReducer
    }),
    composeEnhancers(applyMiddleware(thunk))
  );

  return store;
};
