import { mealConstants } from "../constants/meal";

export default (state = { list: [], busy: false }, action) => {
  console.log("test reducer", state, action);
  switch (action.type) {
    case mealConstants.BUSY:
      return { ...state, busy: action.busy };
    case mealConstants.SET_MEALS_SUCCESS:
      return { ...state, list: [...action.list], busy: false };
    default:
      return state;
  }
};
