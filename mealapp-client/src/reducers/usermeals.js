import { usersMealsConstants } from "../constants/usermeals";

export default (state = { list: [], busy: false }, action) => {
  switch (action.type) {
    case usersMealsConstants.BUSY:
      return { ...state, busy: action.busy };
    case usersMealsConstants.SET_USERS_MEALS_SUCCESS:
      return { ...state, list: [...action.list], busy: false };
    default:
      return state;
  }
};
