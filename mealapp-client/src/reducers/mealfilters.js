import moment from "moment";

const filtersReducerDefaultState = {
  text: "",
  sortBy: "date",
  fromDate: moment()
    .startOf("month")
    .toDate(),
  toDate: moment()
    .endOf("month")
    .toDate(),
  fromTime: moment()
    .startOf("day")
    .toDate(),
  toTime: moment()
    .endOf("day")
    .toDate(),
  user: undefined
};

export default (state = filtersReducerDefaultState, action) => {
  switch (action.type) {
    case "SET_TEXT_FILTER":
      return {
        ...state,
        text: action.text
      };
    case "SORT_BY_CALORIES":
      return {
        ...state,
        sortBy: "calories"
      };
    case "SORT_BY_DATE":
      return {
        ...state,
        sortBy: "date"
      };
    case "SET_FROM_DATE":
      return {
        ...state,
        fromDate: action.fromDate
      };
    case "SET_TO_DATE":
      return {
        ...state,
        toDate: action.toDate
      };
    case "SET_FROM_TIME":
      return {
        ...state,
        fromTime: action.fromTime
      };
    case "SET_TO_TIME":
      return {
        ...state,
        toTime: action.toTime
      };
    case "SET_USER":
      return {
        ...state,
        user: action.user
      };
    default:
      return state;
  }
};
