import { usersConstants } from "../constants/users";

export default (state = { list: [], busy: false }, action) => {
  switch (action.type) {
    case usersConstants.BUSY:
      return { ...state, busy: action.busy };
    case usersConstants.SET_USERS_SUCCESS:
      return { ...state, list: [...action.list], busy: false };
    default:
      return state;
  }
};
