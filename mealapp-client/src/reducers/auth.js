import { authConstants } from "../constants/auth";

export default (state = {}, action) => {
  switch (action.type) {
    case authConstants.LOGIN_SUCCESS:
      return {
        uid: action.uid,
        roles: [action.role],
        busy: false
      };
    case authConstants.LOGOUT:
      return {};
    case authConstants.GET_PROFILE_SUCCESS:
      return { ...state, user: action.profile, busy: false };
    case authConstants.EDIT_PROFILE_SUCCESS:
      return { ...state, user: action.profile, busy: false };
    case authConstants.BUSY:
      return { ...state, busy: action.busy };
    default:
      return state;
  }
};
