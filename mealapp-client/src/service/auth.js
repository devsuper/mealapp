import { apiConstants } from "../constants/api";
import { request } from "./utils";

export const auth = {
  login,
  signUp,
  getProfile,
  updateProfile
};

export function login(username, password) {
  return request({
    url: `${apiConstants.API_BASE_URL}/auth/signin`,
    method: "POST",
    body: JSON.stringify({ usernameOrEmail: username, password })
  });
}

export function signUp(user) {
  return request({
    url: `${apiConstants.API_BASE_URL}/auth/signup`,
    method: "POST",
    body: JSON.stringify(user)
  });
}

export function getProfile() {
  return request({
    url: `${apiConstants.API_BASE_URL}/auth/profile`,
    method: "GET"
  });
}

export function updateProfile(user) {
  return request({
    url: `${apiConstants.API_BASE_URL}/auth/profile`,
    method: "PUT",
    body: JSON.stringify(user)
  });
}
