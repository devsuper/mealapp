import { apiConstants } from "../constants/api";
import { request } from "./utils";

export const usersmeals = {
  getAll,
  create,
  update,
  remove
};

export function getAll() {
  return request({
    url: `${apiConstants.API_BASE_URL}/users/meals/`,
    method: "GET"
  });
}

export function create(meal) {
  return request({
    url: `${apiConstants.API_BASE_URL}/users/meals/`,
    method: "POST",
    body: JSON.stringify(meal)
  });
}

export function update(id, meal) {
  return request({
    url: `${apiConstants.API_BASE_URL}/users/meals/${id}`,
    method: "PUT",
    body: JSON.stringify(meal)
  });
}

export function remove(id) {
  return request({
    url: `${apiConstants.API_BASE_URL}/users/meals/${id}`,
    method: "DELETE"
  });
}
