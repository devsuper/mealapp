import { apiConstants } from "../constants/api";
import { request } from "./utils";

export const meals = {
  getAll,
  create,
  update,
  remove
};

export function getAll() {
  return request({
    url: `${apiConstants.API_BASE_URL}/meals/`,
    method: "GET"
  });
}

export function create(meal) {
  return request({
    url: `${apiConstants.API_BASE_URL}/meals/`,
    method: "POST",
    body: JSON.stringify(meal)
  });
}

export function update(id, meal) {
  return request({
    url: `${apiConstants.API_BASE_URL}/meals/${id}`,
    method: "PUT",
    body: JSON.stringify(meal)
  });
}

export function remove(id) {
  return request({
    url: `${apiConstants.API_BASE_URL}/meals/${id}`,
    method: "DELETE"
  });
}
