import { apiConstants } from "../constants/api";
import { logout } from "./auth";

export const request = options => {
  const headers = new Headers({
    "Content-Type": "application/json"
  });

  if (sessionStorage.getItem(apiConstants.ACCESS_TOKEN)) {
    headers.append(
      "Authorization",
      "Bearer " + sessionStorage.getItem(apiConstants.ACCESS_TOKEN)
    );
  }

  const defaults = { headers: headers };
  options = Object.assign({}, defaults, options);

  return fetch(options.url, options).then(response => handleResponse(response));
};

function handleResponse(response) {
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}
