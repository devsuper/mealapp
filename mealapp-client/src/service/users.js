import { apiConstants } from "../constants/api";
import { request } from "./utils";

export const users = {
  getAll,
  create,
  update,
  remove
};

export function getAll() {
  return request({
    url: `${apiConstants.API_BASE_URL}/users/`,
    method: "GET"
  });
}

export function create(user) {
  return request({
    url: `${apiConstants.API_BASE_URL}/users/`,
    method: "POST",
    body: JSON.stringify(user)
  });
}

export function update(id, user) {
  return request({
    url: `${apiConstants.API_BASE_URL}/users/${id}`,
    method: "PUT",
    body: JSON.stringify(user)
  });
}

export function remove(id) {
  return request({
    url: `${apiConstants.API_BASE_URL}/users/${id}`,
    method: "DELETE"
  });
}
