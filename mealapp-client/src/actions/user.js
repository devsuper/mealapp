import { users } from "../service/users";
import { usersConstants } from "../constants/users";

const busy = busy => ({
  type: usersConstants.BUSY,
  busy
});

const setUsersSuccess = users => ({
  type: usersConstants.SET_USERS_SUCCESS,
  list: users
});

export const startGetUsers = () => {
  return dispatch => {
    dispatch(busy(true));
    return users.getAll().then(
      users => {
        dispatch(setUsersSuccess(users));
      },
      error => {
        dispatch(busy(false));
        throw { errorMsg: "Error getting users", error: error };
      }
    );
  };
};

export const startAddUser = user => {
  return dispatch => {
    dispatch(busy(true));
    return users.create(user).then(
      () => {
        dispatch(busy(false));
        dispatch(startGetUsers());
        return "User created with success!";
      },
      error => {
        dispatch(busy(false));
        throw { errorMsg: "Error creating user", error: error };
      }
    );
  };
};

export const startEditUser = (id, user) => {
  return dispatch => {
    dispatch(busy(true));
    return users.update(id, user).then(
      () => {
        dispatch(busy(false));
        dispatch(startGetUsers());
        return "User edited with success!";
      },
      error => {
        dispatch(busy(false));
        throw { errorMsg: "Error editing user", error: error };
      }
    );
  };
};

export const startRemoveUser = id => {
  return dispatch => {
    dispatch(busy(true));
    return users.remove(id).then(
      () => {
        dispatch(busy(false));
        dispatch(startGetUsers());
        return "User delete with success!";
      },
      error => {
        dispatch(busy(false));
        throw { errorMsg: "Error deleting user", error: error };
      }
    );
  };
};
