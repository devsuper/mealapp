// SET_TEXT_FILTER
export const setTextFilter = (text = "") => ({
  type: "SET_TEXT_FILTER",
  text
});

// SORT_BY_DATE
export const sortByDate = () => ({
  type: "SORT_BY_DATE"
});

// SORT_BY_CALORIES
export const sortByCalories = () => ({
  type: "SORT_BY_CALORIES"
});

export const setFromDate = fromDate => ({
  type: "SET_FROM_DATE",
  fromDate
});

export const setToDate = toDate => ({
  type: "SET_TO_DATE",
  toDate
});

export const setFromTime = fromTime => ({
  type: "SET_FROM_TIME",
  fromTime
});

export const setToTime = toTime => ({
  type: "SET_TO_TIME",
  toTime
});

export const setUser = user => ({
  type: "SET_USER",
  user
});
