import { auth } from "../service/auth";
import { apiConstants } from "../constants/api";
import { authConstants } from "../constants/auth";

const busy = busy => ({
  type: authConstants.BUSY,
  busy
});

export const login = (uid, role) => ({
  type: authConstants.LOGIN_SUCCESS,
  uid,
  role
});

export const startLogin = (username, password) => {
  return dispatch => {
    dispatch(busy(true));
    return auth.login(username, password).then(
      user => {
        dispatch(busy(false));
        sessionStorage.setItem(apiConstants.ACCESS_TOKEN, user.accessToken);
        sessionStorage.setItem(apiConstants.ACCESS_ROLES, user.role);
        dispatch(login(user.accessToken, user.role));
        dispatch(startGetProfile());
        return "Login with success!";
      },
      error => {
        dispatch(busy(false));
        throw { errorMsg: "Error with login", error: error };
      }
    );
  };
};

const logout = () => ({
  type: authConstants.LOGOUT
});

export const startLogout = () => {
  sessionStorage.removeItem(apiConstants.ACCESS_TOKEN);
  sessionStorage.removeItem(apiConstants.ACCESS_ROLES);
  return logout();
};

const getProfileSuccess = profile => ({
  type: authConstants.GET_PROFILE_SUCCESS,
  profile
});

export const startGetProfile = () => {
  return dispatch => {
    dispatch(busy(true));
    auth.getProfile().then(
      profile => {
        dispatch(getProfileSuccess(profile));
      },
      error => {
        dispatch(busy(false));
        console.log("Error getting profile", error);
      }
    );
  };
};

const editProfile = (uid, profile) => ({
  type: authConstants.EDIT_PROFILE_SUCCESS,
  uid,
  profile
});

export const startEditProfile = profile => {
  return dispatch => {
    dispatch(busy(true));
    return auth.updateProfile(profile).then(
      () => {
        dispatch(busy(false));
        dispatch(editProfile(profile));
        return "Profile updated with success!";
      },
      error => {
        dispatch(busy(false));
        throw { errorMsg: "Error updating profile", error: error };
      }
    );
  };
};

export const startSignUpRequst = (name, username, password, email) => {
  return () => {
    return auth.signUp(name, username, password, email).then(
      () => {
        return "User created with success!";
      },
      error => {
        throw { errorMsg: "Error signing you up", error: error };
      }
    );
  };
};
