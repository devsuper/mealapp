import { meals } from "../service/meal";
import { mealConstants } from "../constants/meal";

const busy = busy => ({
  type: mealConstants.BUSY,
  busy
});

const setMealsSuccess = meals => ({
  type: mealConstants.SET_MEALS_SUCCESS,
  list: meals
});

export const startGetMeals = () => {
  return dispatch => {
    dispatch(busy(true));
    meals.getAll().then(
      meals => {
        dispatch(setMealsSuccess(meals));
      },
      error => {
        throw { errorMsg: "Error getting meals", error: error };
      }
    );
  };
};

export const startAddMeal = meal => {
  return dispatch => {
    dispatch(busy(true));
    return meals.create(meal).then(
      () => {
        dispatch(busy(false));
        dispatch(startGetMeals());
        return "Meal created with success!";
      },
      error => {
        dispatch(busy(false));
        throw { errorMsg: "Error creating meal", error: error };
      }
    );
  };
};

export const startEditMeal = (id, meal) => {
  return dispatch => {
    dispatch(busy(true));
    return meals.update(id, meal).then(
      () => {
        dispatch(busy(false));
        dispatch(startGetMeals());
        return "Meal edited with success!";
      },
      error => {
        dispatch(busy(false));
        throw { errorMsg: "Error editing meal", error: error };
      }
    );
  };
};

export const startRemoveMeal = id => {
  return dispatch => {
    dispatch(busy(true));
    return meals.remove(id).then(
      () => {
        dispatch(busy(false));
        dispatch(startGetMeals());
        return "Meal delete with success!";
      },
      error => {
        dispatch(busy(false));
        throw { errorMsg: "Error deleting meal", error: error };
      }
    );
  };
};
