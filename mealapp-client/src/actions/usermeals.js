import { usersmeals } from "../service/usermeals";
import { usersMealsConstants } from "../constants/usermeals";

const busy = busy => ({
  type: usersMealsConstants.BUSY,
  busy
});

const setUserMealsSuccess = meals => ({
  type: usersMealsConstants.SET_USERS_MEALS_SUCCESS,
  list: meals
});

export const startGetUserMeals = () => {
  return dispatch => {
    dispatch(busy(true));
    return usersmeals.getAll().then(
      meals => {
        dispatch(setUserMealsSuccess(meals));
      },
      error => {
        throw { errorMsg: "Error getting user meals", error: error };
      }
    );
  };
};

export const startAddUserMeal = meal => {
  return dispatch => {
    dispatch(busy(true));
    return usersmeals.create(meal).then(
      () => {
        dispatch(busy(false));
        return "Meal created with success!";
      },
      error => {
        dispatch(busy(false));
        throw { errorMsg: "Error creating meal", error: error };
      }
    );
  };
};

export const startEditUserMeal = (id, meal) => {
  return dispatch => {
    dispatch(busy(true));
    return usersmeals.update(id, meal).then(
      () => {
        dispatch(busy(false));
        return "Meal edited with success!";
      },
      error => {
        dispatch(busy(false));
        throw { errorMsg: "Error editing meal", error: error };
      }
    );
  };
};

export const startRemoveUserMeal = id => {
  return dispatch => {
    dispatch(busy(true));
    return usersmeals.remove(id).then(
      () => {
        dispatch(busy(false));
        return "Meal delete with success!";
      },
      error => {
        dispatch(busy(false));
        throw { errorMsg: "Error deleting meal", error: error };
      }
    );
  };
};
