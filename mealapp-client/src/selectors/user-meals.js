import moment from "moment";

export default (
  meals,
  { text, sortBy, fromDate, toDate, fromTime, toTime, user }
) => {
  return meals
    .filter(meal => {
      const createdAt = moment(meal.date);
      const fromDateMatch = fromDate
        ? moment(fromDate).isSameOrBefore(createdAt, "day")
        : true;
      const toDateMatch = toDate
        ? moment(toDate).isSameOrAfter(createdAt, "day")
        : true;

      const createAtTime = minutes(moment(meal.date));

      const fromTimeMatch = fromTime
        ? minutes(moment(fromTime)) <= createAtTime
        : true;
      const toTimeMatch = toTime
        ? minutes(moment(toTime)) >= createAtTime
        : true;

      const textMatch = meal.description
        .toLowerCase()
        .includes(text.toLowerCase());

      const userMatch =
        user && user.id ? user.id === -1 || user.id === meal.user.id : true;

      return (
        toTimeMatch &&
        fromTimeMatch &&
        toDateMatch &&
        fromDateMatch &&
        textMatch &&
        userMatch
      );
    })
    .sort((a, b) => {
      if (sortBy === "date") {
        return a.date < b.date ? 1 : -1;
      } else if (sortBy === "calories") {
        return a.calories < b.calories ? 1 : -1;
      }
    });
};

const minutes = function(m) {
  return m.minutes() + m.hours() * 60;
};
