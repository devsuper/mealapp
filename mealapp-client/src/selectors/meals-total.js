export default meals => {
  return meals
    .map(meal => meal.calories)
    .reduce((sum, value) => sum + value, 0);
};
