package com.toptal.mealapp.service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.toptal.mealapp.model.Meal;
import com.toptal.mealapp.model.User;
import com.toptal.mealapp.payload.MealRequest;
import com.toptal.mealapp.payload.MealResponse;
import com.toptal.mealapp.payload.UserResponse;
import com.toptal.mealapp.repository.MealRepository;
import com.toptal.mealapp.repository.UserRepository;

@Service
public class MealService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private MealRepository mealRepository;

	public Meal createMeal(MealRequest mealRequest, User user) {
		Meal meal = new Meal();
		meal.setDescription(mealRequest.getDescription());
		meal.setDate(mealRequest.getDate());
		meal.setNumOfCalories(mealRequest.getNumOfCalories());
		meal.setCreatedBy(meal.getCreatedBy());
		meal.setUser(user);

		return mealRepository.save(meal);
	}

	public List<MealResponse> getAllMealsByUser(User user) {
		List<Meal> meals = mealRepository.findByUserId(user.getId());
		checkMealsCaloriesExceed(meals, user.getMaxCaloriesPerDay());

		return meals.stream()
				.map(meal -> new MealResponse(meal.getId(), meal.getDescription(), meal.getNumOfCalories(),
						meal.getExceedDaily(), meal.getDate(),
						new UserResponse(meal.getUser().getId(), meal.getUser().getName())))
				.collect(Collectors.toList());
	}

	public List<MealResponse> getAllMeals() {
		List<Meal> meals = mealRepository.findAll();

		Map<Long, List<Meal>> mealsPerUser = meals.stream()
				.collect(Collectors.groupingBy(meal -> meal.getUser().getId()));
		mealsPerUser.forEach((userId, mealList) -> {
			User user = userRepository.findById(userId).get();
			checkMealsCaloriesExceed(mealList, user.getMaxCaloriesPerDay());
		});

		return meals.stream()
				.map(meal -> new MealResponse(meal.getId(), meal.getDescription(), meal.getNumOfCalories(),
						meal.getExceedDaily(), meal.getDate(),
						new UserResponse(meal.getUser().getId(), meal.getUser().getName())))
				.collect(Collectors.toList());
	}

	private void checkMealsCaloriesExceed(List<Meal> meals, Integer maxCaloriesPerDay) {
		Map<String, List<Meal>> mealsPerDay = meals.stream().collect(Collectors.groupingBy(Meal::getDayString));

		mealsPerDay.forEach((day, mealsGroup) -> {
			Integer sumOfDay = mealsGroup.stream().mapToInt(Meal::getNumOfCalories).sum();
			mealsGroup.forEach(meal -> meal.setExceedDaily(sumOfDay > maxCaloriesPerDay));
		});
	}

	public Meal updateMeal(Meal meal, MealRequest mealRequest, User user) {
		meal.setDescription(mealRequest.getDescription());
		meal.setNumOfCalories(mealRequest.getNumOfCalories());
		meal.setDate(mealRequest.getDate());
		meal.setUser(user);
		return mealRepository.save(meal);
	}
}
