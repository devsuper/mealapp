package com.toptal.mealapp.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.toptal.mealapp.model.Role;
import com.toptal.mealapp.model.User;
import com.toptal.mealapp.payload.UserRequest;
import com.toptal.mealapp.payload.UserSummary;
import com.toptal.mealapp.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public List<UserSummary> getUsers() {
		List<User> users = userRepository.findAll();
		return users.stream().map(user -> new UserSummary(user.getId(), user.getUsername(), user.getName(),
				user.getEmail(), user.getMaxCaloriesPerDay(), user.getRole().getName().toString()))
				.collect(Collectors.toList());
	}

	public User updateUser(User user, UserRequest userRequest) {
		user.setName(userRequest.getName());
		user.setEmail(userRequest.getEmail());
		user.setUsername(userRequest.getUsername());
		user.setMaxCaloriesPerDay(userRequest.getMaxCaloriesPerDay());
		return userRepository.save(user);
	}

	public User updateUser(User user, UserRequest userRequest, Role role) {
		user.setName(userRequest.getName());
		user.setEmail(userRequest.getEmail());
		user.setUsername(userRequest.getUsername());
		user.setRole(role);
		return userRepository.save(user);
	}
}
