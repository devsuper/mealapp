package com.toptal.mealapp.util;

import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.toptal.mealapp.model.Meal;
import com.toptal.mealapp.model.Role;
import com.toptal.mealapp.model.RoleName;
import com.toptal.mealapp.model.User;
import com.toptal.mealapp.repository.MealRepository;
import com.toptal.mealapp.repository.RoleRepository;
import com.toptal.mealapp.repository.UserRepository;

@Component
@ConditionalOnProperty(name = "app.db-init", havingValue = "true")
public class DatabaseInitializer implements CommandLineRunner {
	
	private static final Logger logger = LoggerFactory.getLogger(DatabaseInitializer.class);
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private MealRepository mealRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
    public void run(String... strings) throws Exception {
		roleRepository.deleteAll();
		Role userRole = roleRepository.save(new Role(RoleName.ROLE_USER));
		Role adminRole = roleRepository.save(new Role(RoleName.ROLE_ADMIN));
		Role managerRole = roleRepository.save(new Role(RoleName.ROLE_MANAGER));
		
		userRepository.deleteAll();
		User user = userRepository.save(new User("User User", "user", "user@user.pt", passwordEncoder.encode("password"), userRole));
		User manager = userRepository.save(new User("Manager Manager", "manager", "manager@manager.pt", passwordEncoder.encode("password"), managerRole));
		userRepository.save(new User("Admin Admin", "admin", "admin@admin.pt", passwordEncoder.encode("password"), adminRole));
		
		mealRepository.deleteAll();
		mealRepository.save(new Meal("User Meal", Integer.valueOf(1000), Instant.now(), user));
		mealRepository.save(new Meal("User Meal 2", Integer.valueOf(500), Instant.now().minusSeconds(400), user));
		mealRepository.save(new Meal("Manager Meal", Integer.valueOf(7500), Instant.now().minusSeconds(300), manager));
		
		logger.info(" -- Database has been initialized");
	}
}
