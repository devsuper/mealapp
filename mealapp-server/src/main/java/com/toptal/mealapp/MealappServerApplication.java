package com.toptal.mealapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MealappServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MealappServerApplication.class, args);
	}
}
