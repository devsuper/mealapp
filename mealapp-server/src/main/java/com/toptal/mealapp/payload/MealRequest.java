package com.toptal.mealapp.payload;

import java.time.Instant;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class MealRequest {

	@NotBlank
	@Size(max = 300)
	private String description;

	private Integer numOfCalories;

	private Instant date;

	private Long userId;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getNumOfCalories() {
		return numOfCalories;
	}

	public void setNumOfCalories(Integer numOfCalories) {
		this.numOfCalories = numOfCalories;
	}

	public Instant getDate() {
		return date;
	}

	public void setDate(Instant date) {
		this.date = date;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
}
