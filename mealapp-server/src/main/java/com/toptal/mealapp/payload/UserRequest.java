package com.toptal.mealapp.payload;

public class UserRequest extends SignUpRequest {

	private String role;

	private Integer maxCaloriesPerDay;

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Integer getMaxCaloriesPerDay() {
		return maxCaloriesPerDay;
	}

	public void setMaxCaloriesPerDay(Integer maxCaloriesPerDay) {
		this.maxCaloriesPerDay = maxCaloriesPerDay;
	}
}
