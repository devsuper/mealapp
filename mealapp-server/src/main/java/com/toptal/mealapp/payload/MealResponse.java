package com.toptal.mealapp.payload;

import java.time.Instant;

public class MealResponse {
	private Long id;
	private String description;
	private Integer calories;
	private Boolean exceedsDailyCalories;
	private Instant date;
	private UserResponse user;
	
	public MealResponse(Long id, String description, Integer calories, Boolean exceedsDailyCalories, Instant date, UserResponse user) {
		super();
		this.id = id;
		this.description = description;
		this.calories = calories;
		this.exceedsDailyCalories = exceedsDailyCalories;
		this.date = date;
		this.user = user;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getCalories() {
		return calories;
	}
	public void setCalories(Integer calories) {
		this.calories = calories;
	}
	public Boolean getExceedsDailyCalories() {
		return exceedsDailyCalories;
	}
	public void setExceedsDailyCalories(Boolean exceedsDailyCalories) {
		this.exceedsDailyCalories = exceedsDailyCalories;
	}

	public Instant getDate() {
		return date;
	}

	public void setDate(Instant date) {
		this.date = date;
	}

	public UserResponse getUser() {
		return user;
	}

	public void setUser(UserResponse user) {
		this.user = user;
	}
}
