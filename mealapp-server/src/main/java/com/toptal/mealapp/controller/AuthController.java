package com.toptal.mealapp.controller;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.toptal.mealapp.exception.AppException;
import com.toptal.mealapp.exception.BadRequestException;
import com.toptal.mealapp.exception.ResourceNotFoundException;
import com.toptal.mealapp.model.Role;
import com.toptal.mealapp.model.RoleName;
import com.toptal.mealapp.model.User;
import com.toptal.mealapp.payload.ApiResponse;
import com.toptal.mealapp.payload.JwtAuthenticationResponse;
import com.toptal.mealapp.payload.LoginRequest;
import com.toptal.mealapp.payload.SignUpRequest;
import com.toptal.mealapp.payload.UserIdentityAvailability;
import com.toptal.mealapp.payload.UserRequest;
import com.toptal.mealapp.payload.UserSummary;
import com.toptal.mealapp.repository.RoleRepository;
import com.toptal.mealapp.repository.UserRepository;
import com.toptal.mealapp.security.CurrentUser;
import com.toptal.mealapp.security.JwtTokenProvider;
import com.toptal.mealapp.security.UserPrincipal;
import com.toptal.mealapp.service.UserService;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
	
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserService userService;
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JwtTokenProvider tokenProvider;
	
	@PostMapping("/signin")
	public JwtAuthenticationResponse authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsernameOrEmail(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = tokenProvider.generateToken(authentication);
		
		Set<String> roles = authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet());
		Optional<String> role = roles.stream().findFirst();
		
		return new JwtAuthenticationResponse(jwt, role.isPresent() ? role.get() : "");
	}

	@PostMapping("/signup")
	public ApiResponse registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			throw new BadRequestException("Username is already taken!");
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			throw new BadRequestException("Email is already taken!");
		}

		User user = new User(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
				signUpRequest.getPassword());

		user.setPassword(passwordEncoder.encode(user.getPassword()));

		Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
				.orElseThrow(() -> new AppException("User Role not set."));

		user.setRole(userRole);
		userRepository.save(user);

		return new ApiResponse(true, "User registered successfully");
	}
	
    @GetMapping("/profile")
    @PreAuthorize("isAuthenticated()")
    public UserSummary getCurrentUser(@CurrentUser UserPrincipal currentUser) {
    	User user = userRepository.findById(currentUser.getId()).orElseThrow(() -> new ResourceNotFoundException("User", "id", currentUser.getId()));
        return new UserSummary(user.getId(), user.getUsername(), user.getName(), user.getEmail(), user.getMaxCaloriesPerDay(), "");
    }
	
	@PutMapping("/profile")
	@PreAuthorize("isAuthenticated()")  
	public ApiResponse updateUser(@Valid @RequestBody UserRequest userRequest, @CurrentUser UserPrincipal currentUser) {
		User user = userRepository.findById(currentUser.getId()).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", currentUser.getId()));	
		user.setPassword(passwordEncoder.encode(userRequest.getPassword()));
		
		userService.updateUser(user, userRequest);
		
		return new ApiResponse(true, "User Updated Successfully");
	}
    
	@GetMapping("/user/checkUsernameAvailability")
	public UserIdentityAvailability checkUsernameAvailability(@RequestParam(value = "username") String username) {
		Boolean isAvailable = !userRepository.existsByUsername(username);
		return new UserIdentityAvailability(isAvailable);
	}

	@GetMapping("/user/checkEmailAvailability")
	public UserIdentityAvailability checkEmailAvailability(@RequestParam(value = "email") String email) {
		Boolean isAvailable = !userRepository.existsByEmail(email);
		return new UserIdentityAvailability(isAvailable);
	}
}
