package com.toptal.mealapp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.toptal.mealapp.exception.AppException;
import com.toptal.mealapp.exception.BadRequestException;
import com.toptal.mealapp.exception.ResourceNotFoundException;
import com.toptal.mealapp.model.Meal;
import com.toptal.mealapp.model.Role;
import com.toptal.mealapp.model.RoleName;
import com.toptal.mealapp.model.User;
import com.toptal.mealapp.payload.ApiResponse;
import com.toptal.mealapp.payload.MealRequest;
import com.toptal.mealapp.payload.MealResponse;
import com.toptal.mealapp.payload.UserRequest;
import com.toptal.mealapp.payload.UserSummary;
import com.toptal.mealapp.repository.MealRepository;
import com.toptal.mealapp.repository.RoleRepository;
import com.toptal.mealapp.repository.UserRepository;
import com.toptal.mealapp.service.MealService;
import com.toptal.mealapp.service.UserService;

@RestController
@RequestMapping("/api/users")
public class UserController {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private MealRepository mealRepository;
	
	@Autowired
	private MealService mealService;

	@GetMapping
	@PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
	public List<UserSummary> getUsers() {
		return userService.getUsers();
	}
	
	@PostMapping
	@PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
	public ApiResponse createUser(@Valid @RequestBody UserRequest userRequest) {

		if (userRepository.existsByUsername(userRequest.getUsername())) {
			throw new BadRequestException("Username is already taken!");
		}

		if (userRepository.existsByEmail(userRequest.getEmail())) {
			throw new BadRequestException("Username is already taken!");
		}

		RoleName userRoleName = RoleName.valueOf(userRequest.getRole());
		Role userRole = roleRepository.findByName(userRoleName).orElseThrow(() -> new AppException("User Role not set."));

		User user = new User(userRequest.getName(), userRequest.getUsername(), userRequest.getEmail(),userRequest.getPassword());
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		user.setRole(userRole);

		userRepository.save(user);
		
		return new ApiResponse(true, "User Created Successfully");
	}
	
	@PutMapping("/{userId}")
	@PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
	public ApiResponse updateUser(@Valid @RequestBody UserRequest userRequest, @PathVariable(value = "userId") Long userId) {
		User user = userRepository.findById(userId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));
		RoleName userRoleName = RoleName.valueOf(userRequest.getRole());
		Role userRole = roleRepository.findByName(userRoleName).orElseThrow(() -> new AppException("User Role not set."));
	
		user.setPassword(passwordEncoder.encode(userRequest.getPassword()));
		
		userService.updateUser(user, userRequest, userRole);
		
		return new ApiResponse(true, "User Updated Successfully");
	}
	
	@DeleteMapping("/{userId}")
	@PreAuthorize("isAuthenticated()")
	public ApiResponse deleteUser(@PathVariable(value = "userId") Long userId) {
		User user = userRepository.findById(userId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));	
		userRepository.deleteById(user.getId());
	
		return new ApiResponse(true, "User Deleted Successfully");
	}
	
	@DeleteMapping("/{userId}/meals")
	@PreAuthorize("hasRole('ADMIN')")
	public List<MealResponse> getAllMeals(@PathVariable(value = "userId") Long userId) {
		User user = userRepository.findById(userId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));
		return mealService.getAllMealsByUser(user);
	}

	@PostMapping("/{userId}/meals")
	@PreAuthorize("hasRole('ADMIN')")
	public ApiResponse createMeal(@PathVariable(value = "userId") Long userId, @Valid @RequestBody MealRequest mealRequest) {
		User user = userRepository.findById(userId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));
		mealService.createMeal(mealRequest, user);
		
		return new ApiResponse(true, "Meal Created Successfully");
	}

	@PutMapping("/{userId}/meals/{mealId}")
	@PreAuthorize("hasRole('ADMIN')")
	public ApiResponse updateMeal(@PathVariable(value = "userId") Long userId,
			@PathVariable(value = "mealId") Long mealId,
			@Valid @RequestBody MealRequest mealRequest) {

		User user = userRepository.findById(userId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));
		
		Meal meal = mealRepository.findByIdAndUserId(mealId, userId)
				.orElseThrow(() -> new ResourceNotFoundException("Meal", "Id", mealId));
		mealService.updateMeal(meal, mealRequest, user);
		
		return new ApiResponse(true, "Meal Updated Successfully");
	}
	
	@DeleteMapping("/{userId}/meals/{mealId}")
	@PreAuthorize("hasRole('ADMIN')")
	public ApiResponse deleteUserMeal(@PathVariable(value = "userId") Long userId, @PathVariable(value = "mealId") Long mealId) {
		mealRepository.findByIdAndUserId(mealId, userId)
				.orElseThrow(() -> new ResourceNotFoundException("Meal", "Id", mealId));
		
		mealRepository.deleteById(mealId);

		return new ApiResponse(true, "Meal Updated Successfully");
	}
}
