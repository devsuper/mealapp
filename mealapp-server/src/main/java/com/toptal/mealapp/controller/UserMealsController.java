package com.toptal.mealapp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.toptal.mealapp.exception.ResourceNotFoundException;
import com.toptal.mealapp.model.Meal;
import com.toptal.mealapp.model.User;
import com.toptal.mealapp.payload.ApiResponse;
import com.toptal.mealapp.payload.MealRequest;
import com.toptal.mealapp.payload.MealResponse;
import com.toptal.mealapp.repository.MealRepository;
import com.toptal.mealapp.repository.UserRepository;
import com.toptal.mealapp.service.MealService;

@RestController
@RequestMapping("/api/users/meals")
public class UserMealsController {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private MealRepository mealRepository;
	
	@Autowired
	private MealService mealService;
	
	@GetMapping
	@PreAuthorize("hasRole('ADMIN')")
	public List<MealResponse> getAllMeals() {
		return mealService.getAllMeals();
	}

	@PostMapping
	@PreAuthorize("hasRole('ADMIN')")
	public ApiResponse createMeal(@Valid @RequestBody MealRequest mealRequest) {
		User user = userRepository.findById(mealRequest.getUserId()).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", mealRequest.getUserId()));
		
		mealService.createMeal(mealRequest, user);
		
		return new ApiResponse(true, "Meal Created Successfully", null);
	}

	@PutMapping("/{mealId}")
	@PreAuthorize("hasRole('ADMIN')")
	public ApiResponse updateMeal(@Valid @RequestBody MealRequest mealRequest,
			@PathVariable(value = "mealId") Long mealId) {
		User user = userRepository.findById(mealRequest.getUserId()).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", mealRequest.getUserId()));
		
		Meal meal = mealRepository.findById(mealId)
				.orElseThrow(() -> new ResourceNotFoundException("Meal", "Id", mealId));
		
		mealService.updateMeal(meal, mealRequest, user);
		
		return new ApiResponse(true, "Meal Updated Successfully", null);
	}
	
	@DeleteMapping("/{mealId}")
	@PreAuthorize("hasRole('ADMIN')")
	public ApiResponse deleteUserMeal(@PathVariable(value = "mealId") Long mealId) {
		mealRepository.findById(mealId)
				.orElseThrow(() -> new ResourceNotFoundException("Meal", "Id", mealId));
		
		mealRepository.deleteById(mealId);

		return new ApiResponse(true, "Meal Removed Successfully", null);
	}
}
