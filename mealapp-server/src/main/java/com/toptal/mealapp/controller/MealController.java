package com.toptal.mealapp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.toptal.mealapp.exception.ResourceNotFoundException;
import com.toptal.mealapp.model.Meal;
import com.toptal.mealapp.model.User;
import com.toptal.mealapp.payload.ApiResponse;
import com.toptal.mealapp.payload.MealRequest;
import com.toptal.mealapp.payload.MealResponse;
import com.toptal.mealapp.repository.MealRepository;
import com.toptal.mealapp.repository.UserRepository;
import com.toptal.mealapp.security.CurrentUser;
import com.toptal.mealapp.security.UserPrincipal;
import com.toptal.mealapp.service.MealService;

@RestController
@RequestMapping("/api/meals")
public class MealController {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private MealRepository mealRepository;

	@Autowired
	private MealService mealService;

	@GetMapping
	@PreAuthorize("isAuthenticated()")
	public List<MealResponse> getMeals(@CurrentUser UserPrincipal currentUser) {
		User user = userRepository.findById(currentUser.getId()).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", currentUser.getId()));
		return mealService.getAllMealsByUser(user);
	}

	@PostMapping
	@PreAuthorize("isAuthenticated()")
	public ApiResponse createMeal(@Valid @RequestBody MealRequest mealRequest, @CurrentUser UserPrincipal currentUser) {
		User user = userRepository.findById(currentUser.getId()).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", currentUser.getId()));
		mealService.createMeal(mealRequest, user);
		return new ApiResponse(true, "Meal Created Successfully");
	}

	@PutMapping("/{mealId}")
	@PreAuthorize("isAuthenticated()")
	public ApiResponse updateMeal(@Valid @RequestBody MealRequest mealRequest, @CurrentUser UserPrincipal currentUser,
			@PathVariable(value = "mealId") Long mealId) {
		User user = userRepository.findById(currentUser.getId()).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", currentUser.getId()));
		Meal meal = mealRepository.findByIdAndCreatedBy(mealId, currentUser.getId())
				.orElseThrow(() -> new ResourceNotFoundException("Meal", "Id", mealId));
		
		mealService.updateMeal(meal, mealRequest, user);
		return new ApiResponse(true, "Meal Updated Successfully");
	}
	
	@DeleteMapping("/{mealId}")
	@PreAuthorize("isAuthenticated()")
	public ApiResponse deleteMeal(@CurrentUser UserPrincipal currentUser,
			@PathVariable(value = "mealId") Long mealId) {

		Meal meal = mealRepository.findByIdAndUserId(mealId, currentUser.getId())
				.orElseThrow(() -> new ResourceNotFoundException("Meal", "Id", mealId));
		
		mealRepository.deleteById(meal.getId());
		
		return new ApiResponse(true, "Meal Deleted Successfully");
	}
}
