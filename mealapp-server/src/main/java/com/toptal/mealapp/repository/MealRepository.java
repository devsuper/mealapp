package com.toptal.mealapp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.toptal.mealapp.model.Meal;

public interface MealRepository extends JpaRepository<Meal, Long> {

	List<Meal> findByUserId(Long userId);
	
	Optional<Meal> findByIdAndCreatedBy(Long id, Long createdBy);
	Optional<Meal> findByIdAndUserId(Long id, Long userId);
	
    long countByCreatedBy(Long userId);
}
