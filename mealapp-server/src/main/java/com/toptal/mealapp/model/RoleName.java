package com.toptal.mealapp.model;

public enum RoleName {
	ROLE_USER, ROLE_MANAGER, ROLE_ADMIN
}
