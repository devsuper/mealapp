package com.toptal.mealapp.model;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.toptal.mealapp.model.audit.UserDateAudit;

@Entity
@Table(name = "meal")
public class Meal extends UserDateAudit {

	private static final long serialVersionUID = -4071988215426377768L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	private String description;

	@NotNull
	private Integer numOfCalories;

	@NotNull
	private Instant date;

	@ManyToOne(fetch = FetchType.LAZY)
	private User user;
	
	@Transient
	private Boolean exceedDaily;

	public Meal() {

	}

	public Meal(String description, Integer numOfCalories, Instant date) {
		super();
		this.description = description;
		this.numOfCalories = numOfCalories;
		this.date = date;
	}
	
	public Meal(String description, Integer numOfCalories, Instant date, User user) {
		super();
		this.description = description;
		this.numOfCalories = numOfCalories;
		this.date = date;
		this.user = user;
	}

	public String getDayString() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu-MM-dd").withZone(ZoneId.systemDefault());

		return LocalDateTime.ofInstant(date, ZoneId.systemDefault()).format(formatter);
	}

	public Instant getDate() {
		return date;
	}

	public void setDate(Instant date) {
		this.date = date;
	}

	public Integer getNumOfCalories() {
		return numOfCalories;
	}

	public void setNumOfCalories(Integer numOfCalories) {
		this.numOfCalories = numOfCalories;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getExceedDaily() {
		return exceedDaily;
	}

	public void setExceedDaily(Boolean exceedDaily) {
		this.exceedDaily = exceedDaily;
	}
}
